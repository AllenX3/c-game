#include <stdio.h>
#include <stdlib.h> 
#include <conio.h>

int plane_scanf()
{
	int i, j; 
	int x = 5;
	int y = 10; 
	char input;

	while (1)
	{
		system("cls");
		//输出飞机上面的空行
		for(i = 0; i < x; i ++) 
			printf("\n");
		//输出飞机左边的空格
		for( j = 0; j < y; j ++ )
			printf(" ");
		printf("* "); //输出飞机
		printf("\n");
	
		scanf( "%c " , &input);	//根据用户的不同输入来移动
		if(input == 'a') 
			y--;
		if(input == 'd') 
			y++;
		if(input == 'w') 
			x--;
		if (input == 's')
			x++;
	}

	return 0;
}


int plane_getchar()
{
	int i, j; 
	int x = 5; 
	int y = 10; 
	char input; 
	
	while (1)
	{
		system("cls");  //清屏函数
		//输出飞机上面的空行
		for(i=0;i<x;i++) 
			printf("\n");
		//输出飞机左边的空格
		for(j=0;j<y;j++) 
			printf(" ");
		printf("* "); //输出飞机
		printf("\n");

		if(kbhit())   //判断是否有输人
		{
			input = getch(); 
			if(input == 'a') //根据用 户的不同输人来移动，不必输人回车
				y--;  //位置左移
		 	if(input =='d') 
				y++;  //位置右移
			if(input =='w')
				x--;  //位置上移
			if(input =='s') 
				x++;  //位置下移
		}
	}

	return 0;
}

int plane_complex()
{
	int i, j; 
	int x = 5; 
	int y = 10; 
	char input; 
	
	while (1)
	{
		system("cls");  //清屏函数
		//输出飞机上面的空行
		for(i=0;i<x;i++) 
			printf("\n");
		//输出飞机左边的空格
		
		//下面输出一个复杂的飞机图案
		for(j = 0;j<y; j++ ) 
			printf(" ");
		printf("   * \n"); 
		for(j = 0; j<y; j++ )
			printf(" ");
		printf(" ***** \n"); 
		for (j = 0; j<y;j++) 
			printf(" ");
		printf("  * * \n");

		if(kbhit())   //判断是否有输人
		{
			input = getch(); 
			if(input == 'a') //根据用 户的不同输人来移动，不必输人回车
				y--;  //位置左移
		 	if(input =='d') 
				y++;  //位置右移
			if(input =='w')
				x--;  //位置上移
			if(input =='s') 
				x++;  //位置下移
		}
	}

	return 0;
}


int plane_laser()
{
	int i, j; 
	int x = 5; 
	int y = 10; 
	char input; 
	int isFire = 0;
	
	while (1)
	{
		system("cls");  //清屏函数

		if(isFire==0) //输出飞机上面的空行
		{
			for(i=0;i<x;i++) 
				printf("\n");
		}
		else
		{
			for(i=0; i<x; i++)
			{
				for(j = 0;j<y; j++ ) 
					printf(" ");
				printf("   |\n"); 
			}
			isFire = 0;  //发射完后设置开火为0
		}
		
		//下面输出一个复杂的飞机图案
		for(j = 0;j<y; j++ ) 
			printf(" ");
		printf("   * \n"); 
		for(j = 0; j<y; j++ )
			printf(" ");
		printf(" ***** \n"); 
		for (j = 0; j<y;j++) 
			printf(" ");
		printf("  * * \n");

		if(kbhit())   //判断是否有输人
		{
			input = getch(); 
			if(input == 'a') //根据用 户的不同输人来移动，不必输人回车
				y--;  //位置左移
		 	if(input =='d') 
				y++;  //位置右移
			if(input =='w')
				x--;  //位置上移
			if(input =='s') 
				x++;  //位置下移
			if(input ==' ') 
				isFire = 1;  //空格时设置开火为1
		}
	}

	return 0;
}

int plane_target()
{
	int i, j; 
	int x = 5; 
	int y = 10; 
	char input; 
	int isFire = 0;

	int ny = 5;
	int isKilled = 0;
	
	while (1)
	{
		system("cls");  //清屏函数

		if(!isKilled) //输出靶子
		{
			for(j=0;j<ny;j++) 
				printf(" ");
			printf("+\n");
		}

		if(isFire==0) //输出飞机上面的空行
		{
			for(i=0;i<x;i++) 
				printf("\n");
		}
		else
		{
			for(i=0; i<x; i++)
			{
				for(j = 0;j<y; j++ ) 
					printf(" ");
				printf("   |\n"); 
			}
			isFire = 0;  //发射完后设置开火为0
			if(y+2 == ny) //+2是因为激光在飞机的正中间，距最左边2个坐标
				isKilled = 1; // 击中靶子
		}
		
		//下面输出一个复杂的飞机图案
		for(j = 0;j<y; j++ ) 
			printf(" ");
		printf("   * \n"); 
		for(j = 0; j<y; j++ )
			printf(" ");
		printf(" ***** \n"); 
		for (j = 0; j<y;j++) 
			printf(" ");
		printf("  * * \n");

		if(kbhit())   //判断是否有输人
		{
			input = getch(); 
			if(input == 'a') //根据用 户的不同输人来移动，不必输人回车
				y--;  //位置左移
		 	if(input =='d') 
				y++;  //位置右移
			if(input =='w')
				x--;  //位置上移
			if(input =='s') 
				x++;  //位置下移
			if(input ==' ') 
				isFire = 1;  //空格时设置开火为1
		}
	}

	return 0;
}

int main()
{
	int choice, i;

	for (i = 1; i <= 15; i++) {                /* for 的循环体语句开始 */
		printf("Enter choice: ");             /* 输入提示 */
		scanf("%d", &choice);                 /* 接受用户输入的编号 */
											  /* 根据输入的编号，将相应的价格赋给price */
		switch (choice) {
			case 1: plane_scanf(); break;       /* 用break跳出switch语句，下同 */
			case 2: plane_getchar(); break;       /* 用break跳出switch语句，下同 */
			case 3: plane_complex(); break;
			case 4: plane_laser(); break;
			case 5: plane_target(); break;
		
			default: printf("err"); break;
		}
		printf("choice = %d end\n", choice);
	}  
}